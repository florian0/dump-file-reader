#include <cstdio>
#include <fstream>
#include <Windows.h>
#include <iostream>
#include <string>
#include <iomanip>

template<typename T>
void read_binary(T& buf, std::fstream &f)
{
	f.read(reinterpret_cast<char*>(&buf), sizeof(T));
}

int main(int argc, char **argv)
{
	if(argc != 2)
	{
		MessageBox(NULL, "Invalid Parameter Count. Please drag your dump-file onto this binary.", "Error", MB_OK | MB_ICONERROR);
		return -2;
	}

	std::fstream f;
	std::ofstream out;

	
	// Open Dump File
	std::cout << "Opening Dump File: " << argv[1] << std::endl;

	f.open(argv[1], std::ios::in | std::ios::binary);

	if (!f.is_open())
	{
		std::cerr << "Can not open dump file" << std::endl;
		return -1;
	}


	// Open Output File
	std::string outfilename = argv[1];
	outfilename += ".txt";

	std::cout << "Writing Log to : " << outfilename << std::endl;

	out.open(outfilename);

	if (!f.is_open())
	{
		std::cerr << "Can not open output file" << std::endl;
		return -2;
	}

	// Seek to end (there is the offset)
	f.seekg(-4, f.end);

	// Read Offset
	int pos = 0;
	f.read(reinterpret_cast<char*>(&pos), sizeof(pos));

	// Jump to offset
	f.seekg(pos, f.beg);

	// Read Number of Debug Messages
	int dbgnum = 0;
	read_binary(dbgnum, f);

	std::cout << "Number of Messages: " << dbgnum << std::endl;
	out << "Number of Messages: " << dbgnum << std::endl;

	// Read Timestamp
	SYSTEMTIME t;
	read_binary(t, f);

	std::cout << std::setw(2) << std::setfill('0') <<  std::setw(2) << t.wDay << "." <<  std::setw(2) << t.wMonth << "." << t.wYear << " " << std::setw(2) << t.wHour << ":" << std::setw(2) << t.wMinute << ":" << std::setw(2) << t.wSecond << std::endl;
	out << std::setw(2) << std::setfill('0') <<  std::setw(2) << t.wDay << "." <<  std::setw(2) << t.wMonth << "." << t.wYear << " " << std::setw(2) << t.wHour << ":" << std::setw(2) << t.wMinute << ":" << std::setw(2) << t.wSecond << std::endl;


	// Read Number of Debug Messages
	int start = 0;
	read_binary(start, f);

	// Dump Messages
	for (dbgnum; dbgnum > 0; dbgnum--)
	{
		int stamp;
		short len;
		char buf[0xFFFF];

		read_binary(stamp, f);

		read_binary(len, f);

		f.read(buf, len);
		buf[len] = 0;

		std::setprecision(6);

		std::cout << std::setfill(' ') << "[" << std::setw(12) << std::fixed <<  ((stamp - start) / 1000.00) << "] " << buf << std::endl;
		out << std::setfill(' ') << "[" << std::setw(12) << std::fixed <<  ((stamp - start) / 1000.00) << "] " << buf << std::endl;
	}


	f.close();
	out.close();

	getchar();
	return 0;
}
